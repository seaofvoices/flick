#!/bin/sh

set -ex

rojo build -o test-flick.rbxlx test.project.json
run-in-roblox -s scripts/run-tests.lua test-flick.rbxlx
