#!/bin/sh

set -ex

mkdir -p build

rojo build -o build/flick.rbxm
remodel run remove-tests build
