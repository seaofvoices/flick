return function(name, tableToLock)
    return setmetatable(tableToLock, {
        __index = function(_self, key)
            error(('%q is not a valid member of %s'):format(key, name), 2)
        end,
        __newindex = function()
            error(('%s cannot be assigned to'):format(name), 2)
        end,
    })
end
