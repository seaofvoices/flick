return function()
    local Engine = require(script.Parent.Engine)

    local function fakeAnimation(options)
        local update = options.update or function()
            return options.done
        end
        local goalReached = options.goalReached or function() end

        return {
            update = update,
            goalReached = goalReached,
        }
    end

    describe('update', function()
        it('updates all the animations', function()
            local engine = Engine.new()
            local function updateAnim(self, step)
                self.updatedWith = step
                return false
            end
            local firstAnim = fakeAnimation({ update = updateAnim })
            local secondAnim = fakeAnimation({ update = updateAnim })
            local someTime = 0.1

            engine:run(firstAnim)
            engine:run(secondAnim)
            engine:update(someTime)

            expect(firstAnim.updatedWith).to.equal(someTime)
            expect(secondAnim.updatedWith).to.equal(someTime)
        end)

        describe('removes animations that are done', function()
            it('removes the done animation in a list of one', function()
                local engine = Engine.new()
                local anim = fakeAnimation({ done = true })

                engine:run(anim)
                engine:update(0.1)

                expect(#engine.animations).to.equal(0)
            end)

            it('removes all animations when they are all done', function()
                local engine = Engine.new()
                local anim1 = fakeAnimation({ done = true })
                local anim2 = fakeAnimation({ done = true })

                engine:run(anim1)
                engine:run(anim2)

                engine:update(0.1)

                expect(#engine.animations).to.equal(0)
            end)

            it('moves the last animation to the first done', function()
                local engine = Engine.new()
                local anim1 = fakeAnimation({ done = true })
                local anim2 = fakeAnimation({ done = false })

                engine:run(anim1)
                engine:run(anim2)

                engine:update(0.1)

                expect(#engine.animations).to.equal(1)
                expect(engine.animations[1]).to.equal(anim2)
            end)

            it('moves the minimum amount of animations', function()
                local engine = Engine.new()
                local anim1 = fakeAnimation({ done = true })
                local anim2 = fakeAnimation({ done = false })
                local anim3 = fakeAnimation({ done = false })

                engine:run(anim1)
                engine:run(anim2)
                engine:run(anim3)

                engine:update(0.1)

                expect(#engine.animations).to.equal(2)
                expect(engine.animations[1]).to.equal(anim3)
                expect(engine.animations[2]).to.equal(anim2)
            end)
        end)
    end)
end
