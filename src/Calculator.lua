local Primitive = {
    add = function(a, b) return a + b end,
    subtract = function(a, b) return a - b end,
    scalarProduct = function(value, scalar) return scalar * value end,
    lerp = function(start, goal, alpha)
        return start + alpha * (goal - start)
    end,
}

local function lerp(start, goal, alpha)
    return start:Lerp(goal, alpha)
end

local Color3Calculator = {
    add = function(colorA, colorB)
        return Color3.new(colorA.r + colorB.r, colorA.g + colorB.g, colorA.b + colorB.b)
    end,
    subtract = function(colorA, colorB)
        return Color3.new(colorA.r - colorB.r, colorA.g - colorB.g, colorA.b - colorB.b)
    end,
    scalarProduct = function(color, scalar)
        return Color3.new(scalar * color.r, scalar * color.g, scalar * color.b)
    end,
    lerp = lerp,
}

local VectorCalculator = {
    add = Primitive.add,
    subtract = Primitive.subtract,
    scalarProduct = Primitive.scalarProduct,
    lerp = lerp,
}

local UDimCalculator = {
    add = Primitive.add,
    subtract = Primitive.subtract,
    scalarProduct = function(value, scalar)
        return UDim.new(scalar * value.Scale, scalar * value.Offset)
    end,
    lerp = function(start, goal, alpha)
        return UDim.new(
            start.Scale + alpha * (goal.Scale - start.Scale),
            start.Offset + alpha * (goal.Offset - start.Offset)
        )
    end,
}

local UDim2Calculator = {
    add = Primitive.add,
    subtract = Primitive.subtract,
    scalarProduct = function(value, scalar)
        return UDim2.new(
            scalar * value.X.Scale,
            scalar * value.X.Offset,
            scalar * value.Y.Scale,
            scalar * value.Y.Offset
        )
    end,
    lerp = lerp,
}

local typeMap = {
    number = Primitive,
    Color3 = Color3Calculator,
    Vector2 = VectorCalculator,
    Vector3 = VectorCalculator,
    UDim = UDimCalculator,
    UDim2 = UDim2Calculator,
}

return function(value)
    local valueType = typeof(value)

    if valueType == 'string' then
        valueType = value
    end

    local calculator = typeMap[valueType]

    if calculator then
        return calculator
    else
        error(('Flick does not support values of type %q'):format(valueType))
    end
end
