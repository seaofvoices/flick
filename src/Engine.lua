local RunService = game:GetService('RunService')

local function removeIndexes(list, indexes)
    local totalList = #list
    for i = #indexes, 1, -1 do
        local toRemove = indexes[i]

        if toRemove ~= totalList then
            list[toRemove] = list[totalList]
        end

        list[totalList] = nil
        totalList = totalList - 1
    end
end

local Engine = {}

function Engine:start()
    if self.connection then return end

    self.connection = RunService.Heartbeat:Connect(function(step)
        self:update(step)
    end)
end

function Engine:stop()
    if not self.connection then return end

    self.connection:Disconnect()
    self.connection = nil
end

function Engine:run(animation)
    if not self.isRunning[animation] then
        self.isRunning[animation] = true
        table.insert(self.animations, animation)
    end
end

function Engine:stopAnimation(animation)
    if self.isRunning[animation] then
        self.isRunning[animation] = nil
        local animations = self.animations
        local index = table.find(animations, animation)
        if index then
            table.remove(animation, index)
        end
        return true
    end
    return false
end

function Engine:update(step)
    local animations = self.animations
    local isRunning = self.isRunning
    local totalAnimations = #animations
    local totalIndexToRemove = 0
    local indexes = {}

    for i = 1, totalAnimations do
        local animation = animations[i]

        local done = animation:update(step)

        if done then
            isRunning[animation] = nil
            totalIndexToRemove = totalIndexToRemove + 1
            indexes[totalIndexToRemove] = i
            animation:goalReached()
        end
    end

    removeIndexes(animations, indexes)
end

local EngineMetatable = { __index = Engine }

return {
    new = function()
        return setmetatable({
            animations = {},
            isRunning = {},
        }, EngineMetatable)
    end,
}
