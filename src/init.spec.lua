return function()
    local Flick = require(script.Parent)

    describe('api', function()
        it('has withAnimation', function()
            expect(Flick.withAnimation).to.be.a('function')
        end)

        it('has engine provider', function()
            expect(Flick.Provider).to.be.a('table')
        end)

        it('has Motions', function()
            expect(Flick.Motions).to.be.a('table')
        end)
    end)
end
