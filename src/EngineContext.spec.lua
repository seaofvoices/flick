return function()
    local Flick = script.Parent
    local EngineContext = require(Flick.EngineContext)

    local Roact = require(Flick:WaitForChild('FindRoact'))()

    local function getFakeEngine()
        local engine = { running = false }

        function engine:start()
            self.running = true
        end
        function engine:stop()
            self.running = false
        end

        return engine
    end

    describe('api', function()
        it('has a provider', function()
            expect(EngineContext.Provider).to.be.a('table')
        end)

        it('has a consumer', function()
            expect(EngineContext.Consumer).to.be.a('table')
        end)
    end)

    describe('Provider', function()
        local function testAppComponent(props)
            return Roact.createElement(EngineContext.Provider, {
                value = props.engine
            }, {
                Foo = Roact.createElement('Folder')
            })
        end

        it('starts the engine on mount', function()
            local engine = getFakeEngine()

            expect(engine.running).to.equal(false)

            local parent = Instance.new('Folder')
            local testApp = Roact.createElement(testAppComponent, { engine = engine })

            Roact.mount(testApp, parent)

            expect(engine.running).to.equal(true)
        end)

        it('stops the engine on unmount', function()
            local engine = getFakeEngine()

            expect(engine.running).to.equal(false)

            local parent = Instance.new('Folder')
            local testApp = Roact.createElement(testAppComponent, { engine = engine })

            local tree = Roact.mount(testApp, parent)

            expect(engine.running).to.equal(true)

            Roact.unmount(tree)

            expect(engine.running).to.equal(false)
        end)
    end)
end
