local ReplicatedStorage = game:GetService('ReplicatedStorage')
local ReplicatedFirst = game:GetService('ReplicatedFirst')

local potentialParents = {
    script.Parent,
    script.Parent.Parent,
    ReplicatedStorage,
    ReplicatedFirst,
}

local parentListNames = {}

for i = 1, #potentialParents do
    parentListNames[i] = potentialParents[i]:GetFullName()
end

return function()
    for _, parent in pairs(potentialParents) do
        local roactScript = parent:FindFirstChild('Roact')

        if roactScript and roactScript:IsA('ModuleScript') then
            return require(roactScript)
        end
    end

    error(('Flick was unable to find Roact, make sure it is somewhere under: %s'):format(
        table.concat(parentListNames, ', ')
    ))
end
