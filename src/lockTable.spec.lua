return function()
    local lockTable = require(script.Parent.lockTable)

    it('throws when indexing an unknown field', function()
        local t = lockTable('table', {})

        local function shouldThrow()
            return t.foo
        end

        expect(shouldThrow).to.throw('"foo" is not a valid member of table')
    end)

    it('throws when inserting a new field', function()
        local t = lockTable('table', {})

        local function shouldThrow()
            t.foo = true
        end

        expect(shouldThrow).to.throw('table cannot be assigned to')
    end)

    it('does not throw when indexing a field', function()
        local t = lockTable('table', { foo = 10 })

        local function shouldNotThrow()
            return t.foo
        end

        expect(shouldNotThrow).never.to.throw()
    end)
end
