local Flick = script.Parent

local lockTable = require(Flick:WaitForChild('lockTable'))

local FlickClass = {}

function FlickClass:setGoals(goals, goalReachedCallback)
    for name, goalValue in pairs(goals) do
        local animation = self._animations[name]

        if animation then
            animation:setGoal(goalValue, goalReachedCallback and function()
                goalReachedCallback(name)
            end)
            self._engine:run(animation)
        else
            warn(('attempt to set goal of unknown property %q'):format(name))
        end
    end
end

function FlickClass:resetGoals(goalReachedCallback)
    self:setGoals(self._initialValues, goalReachedCallback)
end

function FlickClass:jumpToGoals(goals, goalReachedCallback)
    for name, goalValue in pairs(goals) do
        local animation = self._animations[name]

        if animation then
            animation:jump(goalValue, goalReachedCallback and function()
                goalReachedCallback(name)
            end)
            self._engine:run(animation)
        else
            warn(('attempt to set goal of unknown property %q'):format(name))
        end
    end
end

function FlickClass:loop(goals)
    local animate
    local function animateToInitial()
        self:jumpToGoals(self._initialValues, animate)
    end
    function animate()
        self:setGoals(goals, animateToInitial)
    end
    animate()
end

local FlickMetatable = { __index = lockTable('flick', FlickClass) }

return function(engine, bindings, animations, initialValues)
    local flick = setmetatable({
        bindings = bindings,
        _engine = engine,
        _animations = animations,
        _initialValues = initialValues,
    }, FlickMetatable)

    return flick
end
