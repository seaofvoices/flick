return function()
    local Calculator = require(script.Parent.Calculator)

    local function round(n)
        local integer, fractional = math.modf(n)
        if fractional > 0.5 then
            return integer + 1
        else
            return integer
        end
    end

    local addition = {
        number = {
            addition = {
                values = {1, 3},
                result = 4,
            },
            subtraction = {
                values = {5, 3},
                result = 2,
            },
            multiplication = {
                value = 3,
                scalar = 2,
                result = 6,
            },
            lerp = {
                initial = 1,
                goal = 5,
                alpha = 0.25,
                result = 2,
            },
        },
        Color3 = {
            compare = function(expectedResult, result)
                local expectedColorVector = Vector3.new(
                    round(expectedResult.R * 255),
                    round(expectedResult.G * 255),
                    round(expectedResult.B * 255)
                )
                local colorVector = Vector3.new(
                    round(result.R * 255),
                    round(result.G * 255),
                    round(result.B * 255)
                )

                expect(expectedColorVector).to.equal(colorVector)
            end,
            addition = {
                values = {Color3.fromRGB(1, 2, 3), Color3.fromRGB(2, 3, 4)},
                result = Color3.fromRGB(3, 5, 7),
            },
            subtraction = {
                values = {Color3.fromRGB(7, 11, 5), Color3.fromRGB(2, 1, 4)},
                result = Color3.fromRGB(5, 10, 1),
            },
            multiplication = {
                value = Color3.fromRGB(1, 3, 5),
                scalar = 2,
                result = Color3.fromRGB(2, 6, 10),
            },
            lerp = {
                initial = Color3.fromRGB(1, 8, 10),
                goal = Color3.fromRGB(5, 16, 6),
                alpha = 0.25,
                result = Color3.fromRGB(2, 10, 9),
            },
        },
        UDim = {
            addition = {
                values = {UDim.new(1, 2), UDim.new(2, 3)},
                result = UDim.new(3, 5),
            },
            subtraction = {
                values = {UDim.new(7, 11), UDim.new(2, 1)},
                result = UDim.new(5, 10),
            },
            multiplication = {
                value = UDim.new(1, 3),
                scalar = 2,
                result = UDim.new(2, 6),
            },
            lerp = {
                initial = UDim.new(1, 8),
                goal = UDim.new(5, 16),
                alpha = 0.25,
                result = UDim.new(2, 10),
            },
        },
        UDim2 = {
            addition = {
                values = {UDim2.new(1, 2, 3, 4), UDim2.new(2, 3, 4, 5)},
                result = UDim2.new(3, 5, 7, 9),
            },
            subtraction = {
                values = {UDim2.new(7, 11, 5, 3), UDim2.new(2, 1, 4, 1)},
                result = UDim2.new(5, 10, 1, 2),
            },
            multiplication = {
                value = UDim2.new(1, 3, 5, 7),
                scalar = 2,
                result = UDim2.new(2, 6, 10, 14),
            },
            lerp = {
                initial = UDim2.new(1, 8, 10, 4),
                goal = UDim2.new(5, 16, 6, 0),
                alpha = 0.25,
                result = UDim2.new(2, 10, 9, 3),
            },
        },
        Vector2 = {
            addition = {
                values = {Vector2.new(1, 2), Vector2.new(2, 3)},
                result = Vector2.new(3, 5),
            },
            subtraction = {
                values = {Vector2.new(7, 11), Vector2.new(2, 1)},
                result = Vector2.new(5, 10),
            },
            multiplication = {
                value = Vector2.new(1, 3),
                scalar = 2,
                result = Vector2.new(2, 6),
            },
            lerp = {
                initial = Vector2.new(1, 8),
                goal = Vector2.new(5, 16),
                alpha = 0.25,
                result = Vector2.new(2, 10),
            },
        },
        Vector3 = {
            addition = {
                values = {Vector3.new(1, 2, 3), Vector3.new(2, 3, 4)},
                result = Vector3.new(3, 5, 7),
            },
            subtraction = {
                values = {Vector3.new(7, 11, 5), Vector3.new(2, 1, 4)},
                result = Vector3.new(5, 10, 1),
            },
            multiplication = {
                value = Vector3.new(1, 3, 5),
                scalar = 2,
                result = Vector3.new(2, 6, 10),
            },
            lerp = {
                initial = Vector3.new(1, 8, 10),
                goal = Vector3.new(5, 16, 6),
                alpha = 0.25,
                result = Vector3.new(2, 10, 9),
            },
        },
    }

    for typeName, testValues in pairs(addition) do
        describe(typeName, function()
            local calculator = Calculator(typeName)

            it('adds two values', function()
                local result = calculator.add(unpack(testValues.addition.values))

                if testValues.compare then
                    testValues.compare(result, testValues.addition.result)
                else
                    expect(result).to.equal(testValues.addition.result)
                end
            end)

            it('subtracts two values', function()
                local result = calculator.subtract(unpack(testValues.subtraction.values))

                if testValues.compare then
                    testValues.compare(result, testValues.subtraction.result)
                else
                    expect(result).to.equal(testValues.subtraction.result)
                end
            end)

            it('multiplies with a scalar', function()
                local result = calculator.scalarProduct(
                    testValues.multiplication.value,
                    testValues.multiplication.scalar
                )

                if testValues.compare then
                    testValues.compare(result, testValues.multiplication.result)
                else
                    expect(result).to.equal(testValues.multiplication.result)
                end
            end)

            it('lerps to a value', function()
                local result = calculator.lerp(
                    testValues.lerp.initial,
                    testValues.lerp.goal,
                    testValues.lerp.alpha
                )

                if testValues.compare then
                    testValues.compare(result, testValues.lerp.result)
                else
                    expect(result).to.equal(testValues.lerp.result)
                end
            end)
        end)
    end
end
