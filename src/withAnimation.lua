local Flick = script.Parent

local Roact = require(Flick:WaitForChild('FindRoact'))()
local Animation = require(Flick:WaitForChild('Animation'))
local Motions = require(Flick:WaitForChild('Motions'))
local EngineContext = require(Flick:WaitForChild('EngineContext'))
local lockTable = require(Flick:WaitForChild('lockTable'))
local getFlick = require(Flick:WaitForChild('getFlick'))

local defaultMotionBuilder = Motions.Timed(0.4)

local function withAnimation(options)
    do
        local optionsType = typeof(options)
        assert(
            optionsType == 'table',
            ('bad argument #1 given to Flick.withAnimation(): '
                .. 'options must be a table, got %q'):format(optionsType)
        )
        local initialValuesType = typeof(options.initialValues)
        assert(
            initialValuesType == 'table',
            ('bad argument #1 given to Flick.withAnimation(): options'
                .. ' expected `initialValues` property to be a table, got %q'):format(initialValuesType)
        )
    end
    local initialValues = options.initialValues
    local motionBuilder = options.motion
    if motionBuilder == nil then
        motionBuilder = defaultMotionBuilder
    end

    local function wrapComponent(component)
        local AnimatedComponent = Roact.Component:extend('withAnimation')

        function AnimatedComponent:init()
            self.initialValues = initialValues
            self.bindings = {}
            self.animations = {}

            for name, initialValue in pairs(initialValues) do
                local getValue, updateValue = Roact.createBinding(initialValue)

                local motion = motionBuilder(initialValue)

                self.bindings[name] = getValue
                self.animations[name] = Animation.new(motion, updateValue)
            end

            lockTable('FlickBindings', self.bindings)
            self.lastEngine = nil
            self.lastFlick = nil
        end

        function AnimatedComponent:render()
            local props = self.props

            return Roact.createElement(EngineContext.Consumer, {
                render = function(engine)
                    if engine == nil then
                        error('Cannot find Flick engine. Make sure to wrap your top-level '
                            .. 'component with Flick.EngineContext.Provider')
                    end
                    local flick = self.lastFlick
                    if self.lastEngine ~= engine then
                        if self.lastEngine ~= nil then
                            -- transfer running animations to new engine
                            for _name, animation in pairs(self.animations) do
                                if self.lastEngine:stopAnimation(animation) then
                                    engine:run(animation)
                                end
                            end
                        end

                        flick = getFlick(
                            engine,
                            self.bindings,
                            self.animations,
                            self.initialValues
                        )
                        self.lastFlick = flick
                    end
                    self.lastEngine = engine
                    local newProps = {}
                    for prop, value in pairs(props) do
                        newProps[prop] = value
                    end
                    newProps.flick = flick

                    return Roact.createElement(component, newProps)
                end
            })
        end

        if options.onMount then
            function AnimatedComponent:didMount()
                options.onMount(self.lastFlick)
            end
        end

        function AnimatedComponent:willUnmount()
            local engine = self.lastEngine
            if engine == nil then
                return
            end
            for _name, animation in pairs(self.animations) do
                engine:stopAnimation(animation)
            end
        end

        return AnimatedComponent
    end

    return wrapComponent
end

return withAnimation
