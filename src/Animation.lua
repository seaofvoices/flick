local Animation = {}

function Animation:update(step)
    local done, newValue = self.motion:update(step)
    self.updateBinding(newValue)
    return done
end

function Animation:setGoal(goal, reachedCallback)
    self.reachedCallback = reachedCallback
    self.motion:setGoal(goal)
end

function Animation:jump(goal, reachedCallback)
    self.reachedCallback = reachedCallback
    self.motion:jump(goal)
end

function Animation:goalReached()
    if self.reachedCallback then
        self.reachedCallback()
    end
end

local AnimationMetatable = { __index = Animation }

return {
    new = function(motion, updateBinding)
        return setmetatable({
            motion = motion,
            updateBinding = updateBinding,
        }, AnimationMetatable)
    end,
}
