return function()
    local getFlick = require(script.Parent.getFlick)

    local function getFakeAnimation()
        local animation = {}

        function animation:update(step)
            self.lastStep = step
        end
        function animation:setGoal(goal)
            self.lastGoal = goal
        end

        return animation
    end

    local function getFakeEngine()
        local engine = { animations = {} }

        function engine:run(animation)
            self.animations[animation] = true
        end

        return engine
    end

    describe('bindings', function()
        it('has a field for the bindings', function()
            local bindings = {}
            local flick = getFlick(getFakeEngine(), bindings, {}, {})

            expect(flick.bindings).to.equal(bindings)
        end)
    end)

    describe('setGoals', function()
        it('sets goals for each animation', function()
            local fooAnim = getFakeAnimation()
            local barAnim = getFakeAnimation()
            local animations = {
                foo = fooAnim,
                bar = barAnim,
            }
            local flick = getFlick(getFakeEngine(), {}, animations, {})

            local fooGoal = 3
            local barGoal = 5

            flick:setGoals({
                foo = fooGoal,
                bar = barGoal,
            })

            expect(fooAnim.lastGoal).to.equal(fooGoal)
            expect(barAnim.lastGoal).to.equal(barGoal)
        end)

        it('runs each animation', function()
            local engine = getFakeEngine()
            local fooAnim = getFakeAnimation()
            local barAnim = getFakeAnimation()
            local animations = {
                foo = fooAnim,
                bar = barAnim,
            }
            local flick = getFlick(engine, {}, animations, {})

            local fooGoal = 3
            local barGoal = 5

            flick:setGoals({
                foo = fooGoal,
                bar = barGoal,
            })

            expect(engine.animations[fooAnim]).to.be.ok()
            expect(engine.animations[barAnim]).to.be.ok()
        end)
    end)

    describe('reset', function()
        it('set goals to the initial values', function()
            local fooAnim = getFakeAnimation()
            local barAnim = getFakeAnimation()
            local animations = {
                foo = fooAnim,
                bar = barAnim,
            }
            local initialValues = {
                foo = 3,
                bar = 5,
            }
            local flick = getFlick(getFakeEngine(), {}, animations, initialValues)

            flick:resetGoals()

            expect(fooAnim.lastGoal).to.equal(initialValues.foo)
            expect(barAnim.lastGoal).to.equal(initialValues.bar)
        end)
    end)
end
