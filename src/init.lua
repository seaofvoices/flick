local lockTable = require(script:WaitForChild('lockTable'))

return lockTable('Flick', {
    withAnimation = require(script:WaitForChild('withAnimation')),
    Engine = require(script:WaitForChild('Engine')),
    Motions = require(script:WaitForChild('Motions')),
    Provider = require(script:WaitForChild('EngineContext')).Provider,
})
