local Flick = script.Parent

local Roact = require(Flick:WaitForChild('FindRoact'))()

local engineContext = Roact.createContext(nil)

local WrappedProvider = Roact.Component:extend('EngineProvider')

function WrappedProvider:didMount()
    local engine = self.props.value
    engine:start()
end

function WrappedProvider:render()
    local engine = self.props.value
    assert(engine ~= nil, 'missing prop "value" to flick engine Provider')
    return Roact.createElement(engineContext.Provider, {
        value = engine,
    }, self.props[Roact.Children])
end

function WrappedProvider:willUnmount()
    local engine = self.props.value
    engine:stop()
end

return {
    Consumer = engineContext.Consumer,
    Provider = WrappedProvider,
}
