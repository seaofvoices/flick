return function()
    local Flick = script.Parent
    local withAnimation = require(Flick.withAnimation)
    local Engine = require(Flick.Engine)
    local EngineContext = require(Flick.EngineContext)

    local Roact = require(Flick:WaitForChild('FindRoact'))()

    it('adds a prop named `flick`', function()
        local flickValue = nil

        local function Foo(props)
            flickValue = props.flick
            return nil
        end

        local AnimatedFoo = withAnimation({ initialValues = { value = 0 } })(Foo)

        local tree = Roact.mount(Roact.createElement(EngineContext.Provider, {
            value = Engine.new(),
        }, {
            Foo = Roact.createElement(AnimatedFoo),
        }))

        Roact.unmount(tree)

        expect(flickValue).to.be.ok()
    end)

    it('creates a binding for each initial value', function()
        local flickValue = nil

        local function Foo(props)
            flickValue = props.flick
            return nil
        end

        local initialValues = {
            valueA = 0,
            valueB = 1,
        }
        local AnimatedFoo = withAnimation({ initialValues = initialValues })(Foo)

        local tree = Roact.mount(Roact.createElement(EngineContext.Provider, {
            value = Engine.new(),
        }, {
            Foo = Roact.createElement(AnimatedFoo),
        }))

        Roact.unmount(tree)

        expect(flickValue).to.be.ok()

        local bindings = flickValue.bindings
        expect(bindings).to.be.ok()
        expect(bindings.valueA).to.be.ok()
        expect(bindings.valueB).to.be.ok()
        expect(bindings.valueA:getValue()).to.equal(initialValues.valueA)
        expect(bindings.valueB:getValue()).to.equal(initialValues.valueB)
    end)
end
