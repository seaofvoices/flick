local Flick = script.Parent

local lockTable = require(Flick:WaitForChild('lockTable'))

local Timed = require(script:WaitForChild('TimedMotion'))

local ERROR_MESSAGE = 'Unexpected options passed to Timed motion. Provide a number for the '
    .. 'duration or a table with these fields: { duration: number, easing: "linear" | "quad" }'

return lockTable('Motions', {
    Timed = function(options)
        local optionsType = typeof(options)
        if optionsType == 'number' then
            options = { duration = options }
        elseif optionsType ~= 'table' then
            error(ERROR_MESSAGE)
        end

        local duration = options.duration

        if duration == nil then
            error(ERROR_MESSAGE)
        end

        return function(initialValue)
            return Timed.new(initialValue, initialValue, options)
        end
    end,
})
