local Flick = script.Parent.Parent

local Calculator = require(Flick:WaitForChild('Calculator'))

local TimedMotion = {}

function TimedMotion:update(step)
    local currentTime = self.timer + step
    local duration = self.duration

    if currentTime >= duration then
        self.timer = self.duration
        return true, self.goal
    end

    local initialValue = self.initialValue
    local alpha = self.easing(currentTime, duration)

    self.timer = currentTime

    return false, self.calculator.lerp(initialValue, self.goal, alpha)
end

function TimedMotion:setGoal(goal)
    local _, currentValue = self:update(0)
    self.initialValue = currentValue
    self.goal = goal
    self.timer = 0
end

function TimedMotion:jump(goal)
    self.goal = goal
    self.timer = self.duration
end

local easingToCallback = {
    linear = function(currentTime, duration)
        return currentTime / duration
    end,
    quad = function(currentTime, duration)
        local alpha = currentTime / duration
        return alpha * (2 - alpha)
    end,
}

local TimedMotionMetatable = { __index = TimedMotion }

return {
    new = function(initialValue, goal, options)
        local duration = options.duration
        local easing = options.easing or 'quad'
        local easingCallback = easingToCallback[easing]
        assert(easingCallback ~= nil, ('unknown easing %q'):format(tostring(easing)))

        return setmetatable({
            timer = 0,
            duration = duration,
            initialValue = initialValue,
            goal = goal,
            easing = easingCallback,
            calculator = Calculator(initialValue),
        }, TimedMotionMetatable)
    end,
}
