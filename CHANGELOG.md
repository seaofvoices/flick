# Changelog

## [0.1.0](https://gitlab.com/seaofvoices/flick/-/releases/v0.1.0)

Initial release of Flick.
