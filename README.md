[![pipeline status](https://gitlab.com/seaofvoices/flick/badges/master/pipeline.svg)](https://gitlab.com/seaofvoices/flick/commits/master)
[![license](https://img.shields.io/badge/license-MIT-blue)](LICENSE.txt)

# flick

An animation library for Roact.

# Releases

You can find the different versions of the library on the [release page](https://gitlab.com/seaofvoices/flick/-/releases).

# License

`flick` is available under the MIT license. See [LICENSE.txt](LICENSE.txt) for more details.
